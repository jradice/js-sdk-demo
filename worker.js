
console.log("Loading native bindings...");
importScripts("affdex-native-bindings.js");
console.log("Native bindings loaded.");

Module.addOnInit(function() { 
    console.log("ON INIT - notify main window worker is loaded"); 
    postMessage({"message":"worker_ready"});
});

addEventListener("message", function(evt) {

    if (evt.data && evt.data.message) {
        var message = evt.data.message;
        console.log("WORKER MESSAGE: " + message);

        switch(message) {
            case "start_detector":
                start_detector(evt.data.license);
                break;
            
            case "process_frame":
                process_frame(evt.data.img_data);
                break;
        }
    }
});

var detector = null;
var current_frame_count = 0;

var emotions    = ['joy', 'sadness', 'disgust', 'contempt', 'anger', 'fear', 'surprise'];
var expressions = ['smile', 'innerBrowRaise', 'browRaise', 'browFurrow', 'noseWrinkle', 
                    'upperLipRaise', 'lipCornerDepressor', 'chinRaise', 'lipPucker',
                    'lipPress', 'lipSuck', 'mouthOpen', 'smirk', 'eyeClosure', 'attention'];
                    
function start_detector(license) {

    try {
        console.log("creating new frame detector");
        detector = new Module.FrameDetector(3);
        console.log("created new detector: " + detector);

        detector.setLicenseString(license);
        detector.setDetectAllExpressions(true);
        detector.setDetectAllEmotions(true);

        console.log("STARTING DETECTOR");
        detector.start();
        console.log("DETECTOR STARTED");
        
        postMessage({"message":"detector_started"});
      
    }
    catch (e) {
      console.log("EXCEPTION STARTING DETECTOR: " + e);
    }
}

function process_frame(img_data) {
    var video_width = 320;
    var video_height = 240;
    
    try {
        var num_bytes = video_width * video_height * 4;
        var ptr = Module._malloc(num_bytes);
        var heap_bytes = new Uint8Array(Module.HEAPU8.buffer, ptr, num_bytes);
        heap_bytes.set(img_data);
    
        var frame = new Module.Frame();
        frame.init(video_width, video_height, heap_bytes.byteOffset, Module.COLOR_FORMAT.RGBA, current_frame_count);
    
        console.log("PROCESS FRAME START");
        var faces = detector.process(frame);
        console.log("PROCESS FRAME END")
        Module._free(heap_bytes.byteOffset);
        frame.delete();
    
        var message = {"message":"frame_results"};
        if (faces) {
            var face = faces.get(0);
            if (face) {
                
                if (face.emotions) {
                    message["emotions"] = {}
                    emotions.forEach( function(val, idx) {
                        message["emotions"][val] = face.emotions[val];
                    });
                }
                
                if (face.expressions) {
                    message["expressions"] = {}
                    expressions.forEach( function(val, idx) {
                        message["expressions"][val] = face.expressions[val];
                    });
                }
            }
        }

        postMessage(message);
        current_frame_count++;
    }
    catch(e) {
        console.log("caught an exception: " + e);
        postMessage({"message":"frame_results", "error":e});
    } 
}